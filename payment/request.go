package payment


type GetPaymentsQuery struct {
	PaymentType string `query:"paymentType"`
}
type CreatePaymentRequest struct {
	OrderID int `json:"orderID"`
	CustomerID int `json:"customerID"`
	PaymentTypeID int `json:"paymentTypeID"`
}
type UpdatePaymentRequest struct {
	OrderID int `json:"orderID"`
	CustomerID int `json:"customerID"`
	PaymentTypeID int `json:"paymentTypeID"`
}
