package foods

import (
	"context"
	"database/sql"
)

type IService interface{
	GetFoods() ([]Food, error)
	AddFood(request CreateFoodRequest) (Food, error)
	UpdateFood(foodID int, request UpdateFoodRequest) (Food, error)
	DeleteFood(foodID int) error
	GetFoodByID(foodID int) (Food, error)
}
type Service struct {
	foodRepository IRepository
}
func NewService(repository IRepository) *Service {
	return &Service{foodRepository:repository}
}

func (s *Service) GetFoods() ([]Food, error) {
	foods,err := s.foodRepository.GetFoods(context.Background(), sql.TxOptions{})
	if err != nil {
		return nil, err
	}
	return foods, nil
}
func (s *Service) AddFood(request CreateFoodRequest) (Food, error) {
	food,err := s.foodRepository.AddFood(context.Background(),sql.TxOptions{},request)
	if err != nil {
		return Food{}, err
	}
	return food, nil
}

func (s *Service) UpdateFood(foodID int , request UpdateFoodRequest) (Food, error) {
	food,err := s.foodRepository.UpdateFood(context.Background(), sql.TxOptions{},foodID, request)
	if err != nil {
		return Food{}, err
	}
	return food, nil
}

func (s *Service) DeleteFood(foodID int) error {
	err:= s.foodRepository.DeleteFood(context.Background(),sql.TxOptions{},foodID)
	return err
}

func (s *Service) GetFoodByID(foodID int) (Food, error) {
	food,err := s.foodRepository.GetFoodByID(context.Background(),sql.TxOptions{}, foodID)
	if err != nil {
		return Food{}, err
	}
	return food, nil
}
