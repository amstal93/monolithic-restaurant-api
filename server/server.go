package server

import (
	"fmt"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"log"
	"os"
	"os/signal"
	"restaurant-api/config"
	"syscall"
)
type Handler interface {
	RegisterRoutes(app *fiber.App)
}
type Server struct {
	app *fiber.App
	config config.Server
}
func NewServer(config config.Server, handlers []Handler) *Server {
	app := fiber.New()
	server := Server{config: config, app: app}
	server.app.Use(cors.New(cors.ConfigDefault))
	server.AddRoutes()
	for _,handler := range handlers{
		handler.RegisterRoutes(server.app)
	}
	return &server
}

func healthCheck(c *fiber.Ctx) error {
	return c.SendStatus(fiber.StatusOK)
}
func (s *Server) AddRoutes(){
	s.app.Get("/health", healthCheck)
}
func (s Server) Run() error {
	shutdownChan := make(chan os.Signal, 1)
	signal.Notify(shutdownChan, syscall.SIGINT, syscall.SIGTERM)
	go func() {
		<-shutdownChan
		err := s.app.Shutdown()
		if err != nil {
			log.Println("Error on shutdown gracefully")
		}
	}()
	fmt.Printf("Configuration Port is :%s",s.config.Port)
	return s.app.Listen(fmt.Sprintf(":%s", s.config.Port))
}

//leetcode