package order

import (
	"context"
	"database/sql"
	"gorm.io/gorm"
)

type Repository struct {
	db * gorm.DB
}
func NewRepository(db * gorm.DB) *Repository {
	return &Repository{db:db}
}
func(r *Repository) GetOrders(ctx context.Context, options sql.TxOptions) ([]Order, error) {
	var orders []Order
	r.db.Transaction(
		func(tx *gorm.DB) error {
			if err := tx.Preload(COOKER).Model(&Order{}).Find(&orders).Error; err != nil {
				return err
			}
			return nil // return nil commits the transaction.Otherwise, any error causes rollback
		})
	return orders, nil
}
func(r *Repository) NewOrder(ctx context.Context, options sql.TxOptions, customerID, foodID, cookerID int ) (Order, error) {
	order := Order{CustomerID: customerID, FoodID: foodID, CookerID: cookerID}
	r.db.Transaction(
		func(tx *gorm.DB) error {  //do some database operations in the transaction (use 'tx' from this point, not 'db')
			if err := tx.Create(&order).Error; err != nil { //after that the orderID will get written into the order variable
				return err
			}
			return nil // return nil commits the transaction.Otherwise, any error causes rollback
		})
	return order, nil
}
func(r *Repository) GetOrderByID(ctx context.Context, options sql.TxOptions, orderID int) (Order, error) {
	var order Order
	err := r.db.Transaction(func(tx *gorm.DB) error {

		//err := tx.Find(&order, orderID)

		//err := tx.Find(&order,"id = ?", orderID)

		//err := tx.Where("id = ?", orderID).Find(&order)

		//querying with structs
		//err := tx.Where(&Order{
		//	ID:         orderID,
		//	CustomerID: 0, // if the fields is zero or empty or false ('', 0, false)
		//	FoodID:     0,
		//}).Find(&order)

		//you can specify the fields of the query struct
		err := tx.Preload(COOKER).Where(&Order{
			ID:         orderID,
			CustomerID: 0, // if the fields is zero or empty or false ('', 0, false)
			FoodID:     0,
		}, "ID").Find(&order).Error
		if err != nil {
			return err
		}
		return nil
	})
	if err != nil {
		return Order{}, err
	}
	return order, nil
}
func(r *Repository) UpdateOrderFoodID(ctx context.Context, options sql.TxOptions, orderID, foodID int) (Order, error) {
	//order, err := r.GetOrderByID(ctx, options, orderID)
	//if err != nil {
	//	return Order{}, err
	//}
	order := Order{ID: orderID}
	r.db.Transaction(func(tx *gorm.DB) error {
		//use if all the fields are filled for updating row.
		//order.FoodID = foodID
		//err:= tx.Save(&order).Error // because the id is known, it can update.
		err := tx.Model(&order).Update("food_id", foodID).Error
		if err != nil {
			return err
		}
		return nil
	})
	updatedOrder,err := r.GetOrderByID(ctx, options, orderID)
	if err != nil {
		return Order{}, err
	}
	return updatedOrder, nil
}
func(r *Repository) DeleteOrder(ctx context.Context, options sql.TxOptions, orderID int) error {

	/*
		db.Delete(&Order{}, "10")
		DELETE from orders where id = "10";
	*/
	/*
		Order's ID is `10`
		order := Order{ID: 10}
		r.db.Delete(&order)
		DELETE from orders where id = 10;
	*/

		//
		//Delete with additional conditions
		//order := Order{ID: orderID}
		//r.db.Where("food_id = ?", 3).Delete(&order)


	/*
		var orders []Order
		r.db.Delete(&orders, []int{1,2,3})
		DELETE FROM orders WHERE id IN (1,2,3);
	*/
	err := r.db.Transaction(func(tx *gorm.DB) error {
		err := tx.Delete(&Order{}, orderID).Error
		if err != nil {
			return err
		}
		return nil
	})
	return err
}

// table name mapping
// delete limit. .Limit(10)
// join ler