package order

import (
	"github.com/gofiber/fiber/v2"
)
type Handler struct{
	service OrderService
}
type OrderService interface{
	GetOrders() ([]Order, error)
	NewOrder(customerID, foodID, cookerID int) (Order, error)
	UpdateOrderFoodID(orderID, foodID int) (Order, error)
	DeleteOrder(orderID int) error
	GetOrderByID(orderID int) (Order, error)
}
func NewHandler(service OrderService) *Handler {
	return &Handler{service: service}
}
func (handler *Handler) GetOrders(c *fiber.Ctx) error {
	orders, err := handler.service.GetOrders()
	if err != nil {
		return c.SendStatus(fiber.StatusNotFound)
	}else {
		return c.Status(fiber.StatusOK).JSON(orders)
	}
}
func (handler *Handler) NewOrder(c *fiber.Ctx) error {
	var request CreateOrderRequest
	if err := c.BodyParser(&request); err!=nil {
		return c.SendStatus(fiber.StatusBadRequest)
	}
	order, err := handler.service.NewOrder(request.CustomerID, request.FoodID, request.CookerID)
	if err != nil {
		return c.SendStatus(fiber.StatusExpectationFailed)
	}
	return c.Status(fiber.StatusCreated).JSON(order)
}
func (handler *Handler) UpdateOrderFoodID(c *fiber.Ctx) error {
	orderID, err := c.ParamsInt("id")
	if err != nil {
		return c.SendStatus(fiber.StatusBadRequest)
	}
	var request UpdateOrderFoodIdRequest
	if err = c.BodyParser(&request); err!= nil {
		return c.SendStatus(fiber.StatusBadRequest)
	}
	updatedOrder, err := handler.service.UpdateOrderFoodID(orderID, request.FoodID)
	if err != nil {
		return c.SendStatus(fiber.StatusExpectationFailed)
	}
	return c.Status(fiber.StatusOK).JSON(updatedOrder)
}
func (handler *Handler) DeleteOrder(c *fiber.Ctx) error {
	orderID, err := c.ParamsInt("id")
	err = handler.service.DeleteOrder(orderID)
	if err != nil {
		return c.SendStatus(fiber.StatusExpectationFailed)
	}
	return c.SendStatus(fiber.StatusOK)
}
func (handler *Handler) GetOrderByID(c *fiber.Ctx) error {
	orderID, err := c.ParamsInt("id")
	if err != nil {
		return c.SendStatus(fiber.StatusBadRequest)
	}
	var order Order
	order, err = handler.service.GetOrderByID(orderID)
	if err != nil {
		return c.SendStatus(fiber.StatusExpectationFailed)
	}
	if order == (Order{}) {
		return c.SendStatus(fiber.StatusNotFound)
	}
	return c.Status(fiber.StatusOK).JSON(order)
}
func(handler *Handler) RegisterRoutes(app *fiber.App) {
	app.Get("/orders",handler.GetOrders)
	app.Get("/orders/:id", handler.GetOrderByID)
	app.Post("/orders", handler.NewOrder)
	app.Put("/orders/:id", handler.UpdateOrderFoodID)
	app.Delete("/orders/:id", handler.DeleteOrder)
}